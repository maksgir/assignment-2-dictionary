%include "lib.inc"
%include "colon.inc"
%include "words.inc"
extern find_word

%assign MAX_WORD_SIZE 256

section .rodata

not_fount_message: db "value not found in dictionary", 0
length_error_message: db "input value length is up to 256", 0


section .bss
input_word: resb MAX_WORD_SIZE


section .text
global _start

print_and_exit:
    call print_string
    call print_newline
    call exit

_start:
    mov rdi, input_word
    mov rsi, MAX_WORD_SIZE
    call read_word
    test rax. rax
    je .length_error
    mov rdi, rax
    mov rsi, first_word 
    call find_word
    test rax, rax
    je .not_found

    pop rdi
    add rdi, rax
    inc rdi
    call print_and_exit
    
    .not_find:
	mov rdi, not_find
	call print_string_and_exit
    .too_long_input:
	mov rdi, too_long_input_message
	call print_string_and_exit
    

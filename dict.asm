%include "lib.inc"
global find_word




section .text


;rdi - zero t string 
;rsi - dict begin pointer 

find_word:
    .loop:
    test rsi, rsi
    je .return_zero
    mov r10, rsi ;save current key marker
    mov r9, [rsi];save next key marker
    add rsi, 8 ;let rsi contain current value marker
    push rsi
    push rdi
    call string_equals
    pop rdi
    pop rsi
    cmp rax, 1
    je .return_pointer
    mov rsi, r9
    jmp .loop

    .return_pointer:
        mov rax, r10
        ret
    .return_zero:
        xor rax, rax
        ret



